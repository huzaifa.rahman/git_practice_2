# Git Practice Task

This is a simple program that says hello to the user in C++

### How to Run Code

- First clone the repo by clicking the blue **Clone** option on top right side and copy the URL **Clone with HTTP**
- In `Terminal` run `git clone < URL >`
- Upon successful clone go to the clone repo by i.e `cd git_practice_2`

### Compile and run a file

- Compile main.cpp using the **GCC** compiler using command `g++ -o <out_put_exec> <file_name>.cpp` where `<out_put_exec>` is output executable which you can run. And `<file_name>` is the name of the .cpp file. 
- To view output run command `./<out_put_exec>`

### Functionality

Program ask user to enter his/her name and then greet the by `hello <name_entered>!`.
